var fs = require('fs'),
	WebSocket = require('ws');

const WEBSOCKET_PORT = process.argv[2] || 8082;

// Websocket Server
var socketServer = new WebSocket.Server({port: WEBSOCKET_PORT, perMessageDeflate: false});
socketServer.connectionCount = 0;
socketServer.on('connection', function(socket, upgradeReq) {
	socketServer.connectionCount++;
	console.log(
		'New WebSocket Connection: ',
		(upgradeReq || socket.upgradeReq).socket.remoteAddress,
		(upgradeReq || socket.upgradeReq).headers['user-agent'],
		'('+socketServer.connectionCount+' total)'
	);
	socket.on('close', function(code, message){
		socketServer.connectionCount--;
		console.log(
			'Disconnected WebSocket ('+socketServer.connectionCount+' total)'
		);
	});
});
socketServer.broadcast = function(data) {
	socketServer.clients.forEach(function each(client) {
		if (client.readyState === WebSocket.OPEN) {
			client.send(data);
		}
	});
};

let worry = 0;
let toSend = '';
let lastSent = '';
let clean_interval = null;
let ws_queue_interval = null;

const cleaner = () => {
    fs.readdir('ssot', (err, files) => {
        if (files.length > 999) {
            for (let i = 0; i < files.length - 40; i++) {
                fs.unlinkSync('./ssot/' + files[i]);
            }
            console.log(files.length - 40 + ' old frames cleaned at: ' + new Date());
        }
    });
}

const looper = () => {
    fs.readdir('ssot', (err, files) => {
        toSend = './ssot/' + files.sort()[files.length-1];
        if (lastSent === toSend) {
            worry += 1;
            if (worry > 20) {
                console.log('ffmpeg not giving new images');
            }
        } else {
            socketServer.broadcast(fs.readFileSync(toSend));
            lastSent = toSend;
            worry = 0;
        }
    });
}

if (clean_interval) clearInterval(clean_interval);
clean_interval = setInterval(cleaner, 5*60*1000);
if (ws_queue_interval) clearInterval(ws_queue_interval);
ws_queue_interval = setInterval(looper, 20);

//console.log('Listening for incomming MPEG-TS Stream on http://127.0.0.1:'+STREAM_PORT+'/<secret>');
console.log('Awaiting WebSocket connections on ws://127.0.0.1:'+WEBSOCKET_PORT+'/');

