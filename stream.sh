cd ssot
ffmpeg -i /dev/video2 -r 4 -f image2 s-%04d.png &
SSHOT_ID=$!
cd ..

node websocket-relay.js &
WSS_ID=$!

echo "ffmpeg running as PID $SSHOT_ID and node at PID $WSS_ID"



# https://unix.stackexchange.com/a/76720

